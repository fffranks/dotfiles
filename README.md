<img src="imagens/Logo2.png">

[![Telegram](https://img.shields.io/badge/My-Telegram-red.svg)](https://t.me/FranklinTech) 
[![i3wm](https://img.shields.io/badge/i3-WM-blue.svg)](https://github.com/fffranks/dotfiles/tree/master/.config/i3)
[![bspwm](https://img.shields.io/badge/bsp-WM-green.svg)](https://github.com/fffranks/dotfiles/tree/master/.config/bspwm)

# `CURRENT SETUP:`
| Types               | Software                                                                                                                           |
| :------------------ | :---------------------------------------------------------------------------------------------------------------------------------- |
| Linux distribution        | [Arch Linux](https://wiki.archlinux.org/index.php/Installation_guide_(Portugu%C3%AAs))                                                                                            |                                                                                       
| Window Manager  | [i3-gaps](https://github.com/Airblader/i3)                                                                                             
| Browser           | [Firefox](https://www.mozilla.org/pt-BR/firefox/new/)                                                                                |
| Program Launcher | [Rofi](https://github.com/DaveDavenport/rofi)                                                                                         |
| Color Schemes| [Dracula-Theme](https://github.com/dracula/dracula-theme)                                                                                         |
| Status Bar     | [Polybar](https://github.com/jaagr/polybar)                                                                                         |
| Music Player    | [Spotify](https://flathub.org/apps/details/com.spotify.Client)     									    |                                					     	 	    |
| Vídeo Player        | [MPV](https://mpv.io/)														    |
| Compositor	      | [Picom](https://github.com/yshui/picom)                                                                    			    |
| File Manager  | [Thunar](https://docs.xfce.org/xfce/thunar/start)                    								    |
| Image viewer | [Feh](http://feh.finalrewind.org/) e [Viewnior](http://siyanpanayotov.com/project/viewnior)                                                                                                   |                                                                                          
| Print Screen     | [Flameshot](https://github.com/lupoDharkael/flameshot)                                                                               |
| Screen Recorder    | [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/)                                                                                                    |
| Terminal Emulator   | [Alacritty](https://github.com/zenixls2/alacritty/blob/ligature/INSTALL.md)                                                                                 |
| Shell               | [ZSH](https://wiki.archlinux.org/index.php/Zsh)                                                                                                         |
| Text editor     | [Vim](https://github.com/vim/vim) e [Howl](https://howl.io/)                                                    							    |
| Monitor configuration    | [ARandR](https://christian.amsuess.com/tools/arandr/)                                                                               |
| Lockscreen    | [Fortune](https://github.com/fffranks/dotfiles/blob/master/.config/polybar/scripts/fortune.sh)                                                                                                  |
| Notification Daemon   | [Dunst](https://github.com/dunst-project/dunst)                                                                                    
| Development   | [VSCode](https://wiki.archlinux.org/index.php/Visual_Studio_Code_(Portugu%C3%AAs))

### Tema: Nord-Theme

## i3-Gaps
<img src="imagens/i3-gaps-arch.png">

## Rofi-Menu
<img src="imagens/i3rofi.png">

## i3-Lock
<img src="imagens/i3lock.png">